# CONTENTS OF THIS FILE

 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Maintainers

# INTRODUCTION

The purpose of this module is to allow an editor to prevent a regularly
recurring update feed from overwriting edited values in an entity.

Consider a site maintaining a list of content populated via the migration api
from an external source. As an example, this might be a feed or spreadsheet.
The external source might maintain and update existing previously imported
content as well, causing nodes to be re-saved with each migration run. An editor
might want to override a single field of a single entity coming from this feed
while leaving the rest of the feed intact.  This module allows them to do that.

By enabling the module for a particular entity bundle and associated fields, the
module will prevent any migrations using the content entity destination from
overwriting that field on that entity, allowing an editor to make changes and
have them persist even if the migration is run again.

# REQUIREMENTS

This module requires the following core modules:
 * Migrate
 * Field

# INSTALLATION

This module may be installed by downloading and extracting the module to your
module directory and enabling it in the extend section of your administrative
interface.

# CONFIGURATION

The bundles and fields that can be overridden are configurable at:

Admin > Configuration > Content Authoring > Migrate Override Settings

# MAINTAINERS

Current maintainers:
 * Michael Lutz (mikelutz) - https://www.drupal.org/u/mikelutz

<?php

namespace Drupal\migrate_override;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\field\FieldConfigInterface;

/**
 * Interface for migrate override manager.
 */
interface OverrideManagerServiceInterface {

  const FIELD_IGNORED = 0;

  const FIELD_LOCKED = 1;

  const FIELD_OVERRIDEABLE = 2;

  const ENTITY_FIELD_LOCKED = 0;

  const ENTITY_FIELD_OVERRIDDEN = 1;

  /**
   * Returns the Field instance override setting from config.
   *
   * @param string $entity_type
   *   The entity type.
   * @param string $bundle
   *   The bundle.
   * @param string $field
   *   The field name.
   *
   * @return int
   *   The setting value for this instance.
   */
  public function fieldInstanceSetting(string $entity_type, string $bundle, string $field): int;

  /**
   * Returns the field setting by entity.
   *
   * @param \Drupal\Core\Entity\ContentEntityInterface $entity
   *   The entity to get the field setting for.
   * @param \Drupal\Core\Field\FieldDefinitionInterface|string $field
   *   The field definition or field name.
   *
   * @return int
   *   The field setting.
   */
  public function entityFieldInstanceSetting(ContentEntityInterface $entity, FieldDefinitionInterface|string $field): int;

  /**
   * Determines if we are overriding this bundle.
   *
   * @param string $entity_type_id
   *   The entity type.
   * @param string $bundle
   *   The bundle.
   *
   * @return bool
   *   True if overrides are enabled for this bundle.
   */
  public function bundleEnabled(string $entity_type_id, string $bundle): bool;

  /**
   * Determines if the entity is overrideable.
   *
   * @param \Drupal\Core\Entity\ContentEntityInterface $entity
   *   The entity.
   *
   * @return bool
   *   True if overrides are enabled for this bundle.
   */
  public function entityBundleEnabled(ContentEntityInterface $entity): bool;

  /**
   * Determines if field exists on bundle.
   *
   * @param string $entity_type_id
   *   The entity.
   * @param string $bundle
   *   The bundle.
   *
   * @return bool
   *   True if field exists.
   */
  public function entityBundleHasField(string $entity_type_id, string $bundle): bool;

  /**
   * Creates the bundle field.
   *
   * @param string $entity_type_id
   *   The entity.
   * @param string $bundle
   *   The bundle.
   *
   * @return \Drupal\field\FieldConfigInterface
   *   The field config.
   */
  public function createBundleField(string $entity_type_id, string $bundle): FieldConfigInterface;

  /**
   * Deletes the bundle field.
   *
   * @param string $entity_type_id
   *   The entity.
   * @param string $bundle
   *   The bundle.
   */
  public function deleteBundleField(string $entity_type_id, string $bundle): void;

  /**
   * Gets an entities status for a field.
   *
   * @param \Drupal\Core\Entity\ContentEntityInterface $entity
   *   The entity.
   * @param string $field_name
   *   The field name.
   * @param int $default
   *   The default.
   *
   * @return int
   *   The default to return.
   */
  public function getEntityFieldStatus(ContentEntityInterface $entity, string $field_name, int $default = OverrideManagerServiceInterface::ENTITY_FIELD_LOCKED): int;

  /**
   * Sets an entities status for a field.
   *
   * @param \Drupal\Core\Entity\ContentEntityInterface $entity
   *   The entity.
   * @param string $field_name
   *   The field name.
   * @param int $status
   *   The status.
   */
  public function setEntityFieldStatus(ContentEntityInterface $entity, string $field_name, int $status): void;

  /**
   * Retrieve option list of overrideable fields.
   *
   * @param \Drupal\Core\Entity\ContentEntityInterface $entity
   *   The entity.
   *
   * @return array
   *   The options list.
   */
  public function getOverridableEntityFields(ContentEntityInterface $entity): array;

}

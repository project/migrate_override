<?php

namespace Drupal\migrate_override;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Config\ImmutableConfig;
use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityDisplayRepositoryInterface;
use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\field\Entity\FieldConfig;
use Drupal\field\Entity\FieldStorageConfig;
use Drupal\field\FieldConfigInterface;
use Drupal\field\FieldStorageConfigInterface;

/**
 * Override Manager Service.
 */
class OverrideManagerService implements OverrideManagerServiceInterface {

  /**
   * The field name to use to store data.
   */
  const FIELD_NAME = 'migrate_override_data';

  /**
   * Drupal\Core\Config\ConfigFactoryInterface definition.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected ConfigFactoryInterface $configFactory;

  /**
   * Drupal\Core\Entity\EntityFieldManagerInterface definition.
   *
   * @var \Drupal\Core\Entity\EntityFieldManagerInterface
   */
  protected EntityFieldManagerInterface $entityFieldManager;

  /**
   * Drupal\Core\Entity\EntityTypeManagerInterface definition.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected EntityTypeManagerInterface $entityTypeManager;

  /**
   * The Entity Display Repository.
   *
   * @var \Drupal\Core\Entity\EntityDisplayRepositoryInterface
   */
  protected EntityDisplayRepositoryInterface $entityDisplayRepository;

  /**
   * Constructs a new OverrideManagerService object.
   */
  public function __construct(ConfigFactoryInterface $config_factory, EntityFieldManagerInterface $entity_field_manager, EntityTypeManagerInterface $entity_type_manager, EntityDisplayRepositoryInterface $entity_display_repository) {
    $this->configFactory = $config_factory;
    $this->entityFieldManager = $entity_field_manager;
    $this->entityTypeManager = $entity_type_manager;
    $this->entityDisplayRepository = $entity_display_repository;
  }

  /**
   * {@inheritdoc}
   */
  public function bundleEnabled(string $entity_type_id, string $bundle): bool {
    $config = $this->getConfig();
    $enabled = $config->get("entities.$entity_type_id.$bundle.migrate_override_enabled");
    if (empty($enabled)) {
      $enabled = FALSE;
    }
    return $enabled;
  }

  /**
   * {@inheritdoc}
   */
  public function entityBundleEnabled(ContentEntityInterface $entity): bool {
    return $this->bundleEnabled($entity->getEntityTypeId(), $entity->bundle());
  }

  /**
   * {@inheritdoc}
   */
  public function fieldInstanceSetting(string $entity_type, string $bundle, string $field): int {
    $config = $this->getConfig();
    $status = $config->get("entities.$entity_type.$bundle.fields.$field");
    if (empty($status)) {
      $status = OverrideManagerServiceInterface::FIELD_IGNORED;
    }
    return $status;
  }

  /**
   * {@inheritdoc}
   */
  public function entityFieldInstanceSetting(ContentEntityInterface $entity, FieldDefinitionInterface|string $field): int {
    if ($field instanceof FieldDefinitionInterface) {
      $field = $field->getName();
    }
    return $this->fieldInstanceSetting($entity->getEntityTypeId(), $entity->bundle(), $field);
  }

  /**
   * {@inheritdoc}
   */
  public function entityBundleHasField(string $entity_type_id, string $bundle): bool {
    if (!$this->entityHasFieldStorage($entity_type_id)) {
      return FALSE;
    }
    $fields = $this->entityFieldManager->getFieldDefinitions($entity_type_id, $bundle);
    return isset($fields[static::FIELD_NAME]);
  }

  /**
   * {@inheritdoc}
   */
  public function createBundleField(string $entity_type_id, string $bundle): FieldConfigInterface {
    if ($this->entityBundleHasField($entity_type_id, $bundle)) {
      return FieldConfig::loadByName($entity_type_id, $bundle, static::FIELD_NAME);
    }
    $field_storage = $this->createFieldStorage($entity_type_id);
    /** @var \Drupal\field\FieldConfigInterface $field */
    $field = $this->entityTypeManager->getStorage('field_config')->create([
      'field_storage' => $field_storage,
      'bundle' => $bundle,
      'label' => 'Select Fields to override:',
      'settings' => [],
    ]);
    $field->save();
    $form_modes = $this->entityDisplayRepository->getFormModeOptionsByBundle($entity_type_id, $bundle);
    foreach (array_keys($form_modes) as $mode) {
      /** @var \Drupal\Core\Entity\Display\EntityFormDisplayInterface $form_mode */
      $form_mode = $this->entityTypeManager->getStorage('entity_form_display')->load($entity_type_id . '.' . $bundle . '.' . $mode);
      $form_mode->setComponent(static::FIELD_NAME, [
        'type' => 'override_widget_default',
      ]);
      $form_mode->save();

    }
    $view_modes = $this->entityDisplayRepository->getViewModeOptionsByBundle($entity_type_id, $bundle);
    foreach (array_keys($view_modes) as $mode) {
      /** @var \Drupal\Core\Entity\Display\EntityViewDisplayInterface $view_mode */
      $view_mode = $this->entityTypeManager->getStorage('entity_view_display')->load($entity_type_id . '.' . $bundle . '.' . $mode);
      $view_mode->setComponent(static::FIELD_NAME, [
        'region' => 'hidden',
      ]);
      $view_mode->save();
    }
    return $field;
  }

  /**
   * {@inheritdoc}
   */
  public function deleteBundleField(string $entity_type_id, string $bundle): void {
    if (!$this->entityBundleHasField($entity_type_id, $bundle)) {
      // Nothing to do.
      return;
    }
    $config = FieldConfig::loadByName($entity_type_id, $bundle, static::FIELD_NAME);
    $config->delete();

  }

  /**
   * {@inheritdoc}
   */
  public function getEntityFieldStatus(ContentEntityInterface $entity, string $field_name, int $default = OverrideManagerServiceInterface::ENTITY_FIELD_LOCKED): int {
    $field_data = $entity->get(static::FIELD_NAME);
    if ($field_data->isEmpty()) {
      return $default;
    }
    $data = $field_data->getValue()[0]['value'] ?? [];
    if (!is_array($data)) {
      $data = unserialize($data, ['allowed_classes' => FALSE]);
    }
    if (!isset($data[$field_name])) {
      return $default;
    }
    return $data[$field_name];
  }

  /**
   * {@inheritdoc}
   */
  public function setEntityFieldStatus(ContentEntityInterface $entity, string $field_name, int $status): void {
    $field_data = $entity->get(static::FIELD_NAME);
    if ($field_data->isEmpty()) {
      $data = [];
    }
    else {
      $data = $field_data->getValue()[0]['value'] ?? [];
      if (!is_array($data)) {
        $data = unserialize($data, ['allowed_classes' => FALSE]);
      }
    }
    $data[$field_name] = $status;
    $entity->set(static::FIELD_NAME, [['value' => serialize($data)]]);
  }

  /**
   * {@inheritdoc}
   */
  public function getOverridableEntityFields(ContentEntityInterface $entity): array {
    return $this->getOverridableFields($entity->getEntityTypeId(), $entity->bundle());
  }

  /**
   * Determines if the entity field storage exists.
   *
   * @param string $entity_type_id
   *   The entity type.
   *
   * @return bool
   *   True if storage exists.
   */
  protected function entityHasFieldStorage(string $entity_type_id): bool {
    $fields = $this->entityFieldManager->getFieldStorageDefinitions($entity_type_id);
    return isset($fields[static::FIELD_NAME]);
  }

  /**
   * Creates field storage if it doesn't exist.
   *
   * @param string $entity_type_id
   *   The entity type id.
   *
   * @return \Drupal\field\FieldStorageConfigInterface
   *   The storage.
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  protected function createFieldStorage(string $entity_type_id): FieldStorageConfigInterface {
    if ($this->entityHasFieldStorage($entity_type_id)) {
      return FieldStorageConfig::loadByName($entity_type_id, static::FIELD_NAME);
    }
    $storage = FieldStorageConfig::create([
      'field_name' => static::FIELD_NAME,
      'entity_type' => $entity_type_id,
      'type' => 'migrate_override_field_item',
    ]);
    $storage->save();
    return $storage;
  }

  /**
   * Returns a field options list for given bundle.
   *
   * @param string $entity_type_id
   *   The entity type id.
   * @param string $bundle
   *   The bundle id.
   *
   * @return array
   *   The options list.
   */
  protected function getOverridableFields(string $entity_type_id, string $bundle): array {
    if (!$this->bundleEnabled($entity_type_id, $bundle)) {
      return [];
    }
    $config = $this->getConfig();
    $fields = $config->get("entities.$entity_type_id.$bundle.fields");
    $field_definitions = $this->entityFieldManager->getFieldDefinitions($entity_type_id, $bundle);
    $options = [];
    foreach ($fields as $field_name => $setting) {
      if ((int) $setting === OverrideManagerServiceInterface::FIELD_OVERRIDEABLE) {
        $options[$field_name] = $field_definitions[$field_name]->getLabel();
      }
    }
    return $options;
  }

  /**
   * Refreshes the config file.
   *
   * @return \Drupal\Core\Config\ImmutableConfig
   *   The settings config.
   */
  protected function getConfig(): ImmutableConfig {
    return $this->configFactory->get('migrate_override.migrateoverridesettings');
  }

}

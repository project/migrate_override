<?php

namespace Drupal\migrate_override\Form;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\ContentEntityType;
use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\Core\Entity\EntityTypeBundleInfoInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\migrate_override\OverrideManagerService;
use Drupal\migrate_override\OverrideManagerServiceInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Form class for migrate override settings.
 *
 * @phpstan-consistent-constructor
 */
class MigrateOverrideSettingsForm extends ConfigFormBase {

  /**
   * Drupal\Core\Entity\EntityTypeManagerInterface definition.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected EntityTypeManagerInterface $entityTypeManager;

  /**
   * The entity type bundle info service.
   *
   * @var \Drupal\Core\Entity\EntityTypeBundleInfoInterface
   */
  protected EntityTypeBundleInfoInterface $entityTypeBundleInfo;

  /**
   * Drupal\Core\Entity\EntityFieldManagerInterface definition.
   *
   * @var \Drupal\Core\Entity\EntityFieldManagerInterface
   */
  protected EntityFieldManagerInterface $entityFieldManager;

  /**
   * The override manager service.
   *
   * @var \Drupal\migrate_override\OverrideManagerServiceInterface
   */
  protected OverrideManagerServiceInterface $overrideManager;

  /**
   * Constructs a MigrateOverrideSettingsForm object.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\Core\Entity\EntityTypeBundleInfoInterface $entity_type_bundle_info
   *   The entity type bundle info service.
   * @param \Drupal\Core\Entity\EntityFieldManagerInterface $entity_field_manager
   *   The entity field manager service.
   * @param \Drupal\migrate_override\OverrideManagerServiceInterface $override_manager
   *   The override manager service.
   */
  public function __construct(ConfigFactoryInterface $config_factory, EntityTypeManagerInterface $entity_type_manager, EntityTypeBundleInfoInterface $entity_type_bundle_info, EntityFieldManagerInterface $entity_field_manager, OverrideManagerServiceInterface $override_manager) {
    parent::__construct($config_factory);
    $this->entityTypeManager = $entity_type_manager;
    $this->entityTypeBundleInfo = $entity_type_bundle_info;
    $this->entityFieldManager = $entity_field_manager;
    $this->overrideManager = $override_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container): ConfigFormBase|MigrateOverrideSettingsForm|static {
    return new static(
      $container->get('config.factory'),
      $container->get('entity_type.manager'),
      $container->get('entity_type.bundle.info'),
      $container->get('entity_field.manager'),
      $container->get('migrate_override.override_manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames(): array {
    return [
      'migrate_override.migrateoverridesettings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string {
    return 'migrate_override_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state): array {
    $config = $this->config('migrate_override.migrateoverridesettings');

    $entity_types = $this->getContentEntityTypes();

    /** @var \Drupal\Core\Entity\EntityTypeInterface $entity_type */
    foreach ($entity_types as $entity_type) {
      $type_id = $entity_type->id();
      $bundles = $this->entityTypeBundleInfo->getBundleInfo($type_id);
      $form['#tree'] = TRUE;
      if ($bundles) {
        $form[$type_id] = [
          '#type' => 'fieldset',
          '#title' => $this->t('@type Entity Type', ['@type' => $entity_type->getLabel()]),
        ];
        foreach ($bundles as $bundle_id => $bundle_info) {
          $fields = $this->entityFieldManager->getFieldDefinitions($type_id, $bundle_id);
          if ($fields) {
            $form[$type_id][$bundle_id]['migrate_override_enabled'] = [
              '#type' => 'checkbox',
              '#title' => $this->t('Enable for @bundle', ['@bundle' => $bundle_info['label']]),
              '#default_value' => $config->get("entities.$type_id.$bundle_id.migrate_override_enabled"),
            ];
            // Bundle Info comes already translated into the current language
            // from the bundle info manager.
            $form[$type_id][$bundle_id]['fields'] = [
              '#type' => 'details',
              '#title' => $bundle_info['label'],
              '#open' => $config->get("entities.$type_id.$bundle_id.migrate_override_enabled"),
            ];
            foreach ($fields as $field_name => $field_definition) {
              $isFormVisible = $field_definition->getDisplayOptions('form') !== NULL
                || $field_definition->isDisplayConfigurable('form');
              if (!$isFormVisible) {
                continue;
              }
              if ($field_name === OverrideManagerService::FIELD_NAME) {
                continue;
              }
              $form[$type_id][$bundle_id]['fields'][$field_name] = [
                '#type' => 'select',
                '#options' => [
                  OverrideManagerServiceInterface::FIELD_IGNORED => $this->t('Ignore this field'),
                  OverrideManagerServiceInterface::FIELD_LOCKED => $this->t('Prevent this field from being overwritten'),
                  OverrideManagerServiceInterface::FIELD_OVERRIDEABLE => $this->t('Allow this field to be overridden'),
                ],
                '#default_value' => $config->get("entities.$type_id.$bundle_id.fields.$field_name"),
                // DataDefinitionInterface->getLabel will return a
                // TranslatableMarkup object if appropiate, so no need to wrap
                // in $this->t()
                '#title' => $field_definition->getLabel(),
              ];
            }
          }
        }
      }
    }

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state): void {
    parent::submitForm($form, $form_state);
    $config = $this->config('migrate_override.migrateoverridesettings');
    $entity_types = $this->getContentEntityTypes();

    /** @var \Drupal\Core\Entity\EntityTypeInterface $entity_type */
    foreach ($entity_types as $entity_type) {
      $type_id = $entity_type->id();
      $bundles = $this->entityTypeBundleInfo->getBundleInfo($type_id);
      if ($bundles) {
        foreach ($bundles as $bundle_id => $bundle_info) {
          $enabled = $form_state->getValue([
            $type_id,
            $bundle_id,
            'migrate_override_enabled',
          ]);
          $config->set("entities.$type_id.$bundle_id.migrate_override_enabled", $enabled);
          if ($enabled) {
            if (!$this->overrideManager->entityBundleHasField($type_id, $bundle_id)) {
              $this->overrideManager->createBundleField($type_id, $bundle_id);
            }
            $fields = $this->entityFieldManager->getFieldDefinitions($type_id, $bundle_id);
            if ($fields) {
              foreach ($fields as $field_name => $field_definition) {
                $config->set("entities.$type_id.$bundle_id.fields.$field_name", $form_state->getValue([
                  $type_id,
                  $bundle_id,
                  'fields',
                  $field_name,
                ]));
              }
            }
          }
          else {
            if ($this->overrideManager->entityBundleHasField($type_id, $bundle_id)) {
              $this->overrideManager->deleteBundleField($type_id, $bundle_id);
            }
          }
        }
      }
    }

    $config->save();
  }

  /**
   * Returns all content entity type definitions.
   *
   * @return \Drupal\Core\Entity\EntityTypeInterface[]
   *   An array of content entity types.
   */
  protected function getContentEntityTypes(): array {
    $all_entity_types = $this->entityTypeManager->getDefinitions();

    $content_entity_types = array_filter(
      $all_entity_types,
      function ($entity_type) {
        return $entity_type instanceof ContentEntityType;
      }
    );
    unset($content_entity_types['migration_data']);

    return $content_entity_types;
  }

}

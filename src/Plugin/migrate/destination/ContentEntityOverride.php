<?php

namespace Drupal\migrate_override\Plugin\migrate\destination;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\migrate\MigrateException;
use Drupal\migrate\Plugin\migrate\destination\EntityContentBase;
use Drupal\migrate\Plugin\MigrationInterface;
use Drupal\migrate\Row;
use Drupal\migrate_override\OverrideManagerService;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Content entity destination.
 *
 * @MigrateDestination(
 *   id = "entity_override",
 *   deriver = "Drupal\migrate_override\Plugin\Derivative\MigrateEntityOverride"
 * )
 */
class ContentEntityOverride extends EntityContentBase {

  /**
   * The override manager.
   *
   * @var \Drupal\migrate_override\OverrideManagerServiceInterface
   */
  protected $overrideManager;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition, MigrationInterface $migration = NULL) {
    $plugin = parent::create($container, $configuration, $plugin_id, $plugin_definition, $migration);
    $plugin->overrideManager = $container->get('migrate_override.override_manager');
    return $plugin;
  }

  /**
   * {@inheritdoc}
   */
  protected static function getEntityTypeId($plugin_id): ?string {
    // Remove "entity_override:".
    return substr($plugin_id, 16);
  }

  /**
   * {@inheritdoc}
   *
   * @throws \Drupal\migrate\MigrateException
   */
  protected function updateEntity(EntityInterface $entity, Row $row): EntityInterface {
    if (!$entity instanceof ContentEntityInterface) {
      throw new MigrateException("Entity Override only works with content entities");
    }
    if ($this->overrideManager->entityBundleEnabled($entity)) {
      $new_row = $row->cloneWithoutDestination();
      foreach ($row->getDestination() as $field_name => $field_value) {
        if ($this->overrideManager->getEntityFieldStatus($entity, $field_name) !== OverrideManagerService::ENTITY_FIELD_OVERRIDDEN) {
          $new_row->setDestinationProperty($field_name, $row->getDestinationProperty($field_name));
        }
      }
      foreach ($row->getEmptyDestinationProperties() as $empty_destination_property) {
        if ($this->overrideManager->getEntityFieldStatus($entity, $empty_destination_property) !== OverrideManagerService::ENTITY_FIELD_OVERRIDDEN) {
          $new_row->setEmptyDestinationProperty($empty_destination_property);
        }
      }
      $row = $new_row;
    }
    return parent::updateEntity($entity, $row);
  }

}
